# Path Planning #

---

## Path Planning Project ##

The goals / steps of this project are the following:

* Safely navigate around a virtual highway with other traffic that is driving ±10 MPH of the 50 MPH speed limit
* The car should try to go as close as possible to the 50 MPH speed limit, which means passing slower traffic when possible
* The car should avoid hitting other cars at all cost as well as driving inside of the marked road lanes at all times
* The car should be able to make one complete loop around the 6946m highway
* The car should not experience total acceleration over 10 m/s^2 and jerk that is greater than 10 m/s^3

## Rubric Points ##

Here I will consider the [rubric points](https://review.udacity.com/#!/rubrics/1020/view) individually and describe how I addressed each point in my implementation.  

---

### Valid Trajectories ###

#### 1. The car is able to drive at least 4.32 miles without incident ####

Since the traffic is generated randomly, it is difficult to guarantee that the car can drive for a certain distance without incident. Statistics from several cold-start tests shows that the car can normally drive for 5 - 10 miles without incident. Most incidents result from other cars rear-ending the ego car when it is having difficulty deciding whether to change lane when two cars are in front at approximately the same speed and/or varying speed.

#### 2. The car drives according to the speed limit ####

To limit the speed of the car, only trajectories with end speed lower than the speed limit are generated to choose from.

#### 3. Max Acceleration and Jerk are not Exceeded ####

To limit the acceleration and jerk, only trajectories with end speed not too far away from current speed are generated to choose from.

#### 4. Car does not have collisions ####

The cost function has a component exponential to how far other cars have intruded the "buffer zone" to avoid coming too close to other cars.

#### 5. The car stays in its lane, except for the time between changing lanes ####

Only trajectories with end points on the lane centers are generated to choose from. Furthermore, a cost is added for lane changes.

#### 6. The car is able to change lanes ####

The cost has a component that is inversely proportional to the car's speed to encourage higher speed, so that the car will change lane if the cost increase resulted from deceleration is greater than the cost for lane change plus cost of collision.

### Reflection / Model Documentation ###

#### 1. Collision avoidance ####

To avoid hitting other vehicles, I initially just used sensor fusion data at each time step, but it wasn't successful since cars behind may become faster than the ego car at some later time. So I stored the sensor fusion data in a map of all other cars, and updated them with a last observed timestamp to calculate the car's position at a later time, assuming constant speed and no lane changes. This setup works most of the time but is somewhat conservative as it may not know that another vehicle at adjacent lane has decelerated and is no longer obstructing lane change trajectories.

More complex cost functions were tested, including treating cars in front and back with different buffer distance, and adding additional cost to cars currently in the blind spots/are side-by-side in adjacent lanes. But they all caused more aggressive maneuvers and collision.

#### 2. Speed and acceleration ####

To ensure smooth speed transition between predictions, a last_speed variable is used and updated at each step to be the end speed of the chosen trajectory. A max_acc limit is set, such that when generating candidate trajectories, only end speeds that are within last_speed ± max_acc * t_interval are available to choose from (also within the 0-50 mph range). After the end speed is chosen, the acceleration is obtained by dividing the speed difference by the time duration. Jerk has not been intentionally minimized but is always less than the limit as a result.

Besides the inversely proportional speed cost, a linear cost proportional to the difference with the target speed was also tested, but the car behaved too aggressively even with a very small weight.

#### 3. Lane change ####

Spline fitting and 5 anchor points were used (adapted from the project walk-through). Target lane candidates are any adjacent lanes plus the current lane. To avoid changing lanes too rapidly, a cost is added for each lane change. But the car still wobbles a lot when the min-cost trajectory changes rapidly between lanes. To reduce wobbling, I tried changing weights and adding another cost for changing lane too rapidly, but none of these approaches worked; they all resulted in an increase of accident rate.

### Potential Improvements ###

* Use finite state machine approach to deal with alternating min-cost trajectory scenario

* Compare long-term cost to tunnel through short-term high-cost configurations (e.g. change to non-adjacent lanes, may sacrifice short-term speed and increase chance of collision)
